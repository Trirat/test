package config

import (
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Config struct {
	App App   `mapstructure:"app"`
	Log Log   `mapstructure:"log"`
	DB  Mysql `mapstructure:"mysql"`
}

type App struct {
	Name    string `mapstructure:"port"`
	Port    int    `mapstructure:"port"`
	Version string `mapstructure:"version"`
	Env     string `mapstructure:"env"`
}

type Log struct {
	Env   string `mapstructure:"env"`
	Level string `mapstructure:"level"`
}

type Mysql struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Database string `mapstructure:"database"`
}

var config Config
var configOnce sync.Once

func InitConfig() Config {
	configOnce.Do(func() {
		log.Println("log", viper.GetString("log.env"))
		viper.AddConfigPath("./config")
		viper.SetConfigName("config")
		viper.SetConfigType("yaml")

		// default here
		viper.SetDefault("http-client.max-idles-con", 100)
		viper.SetDefault("http-client.max-con", 100)
		viper.SetDefault("http-client.idle-conn-timeout", 100)
		viper.SetDefault("http-client.request-timeout", time.Duration(30*time.Second))
		viper.SetDefault("external.example_service.max-retry", 10)

		if err := viper.ReadInConfig(); err != nil {
			log.Fatal("config file not found. using default/env config: " + err.Error())
		}
		viper.AutomaticEnv()

		if err := viper.Unmarshal(&config); err != nil {
			log.Panic(err.Error())
		}
	})
	return config
}

func InitDatabase(cfg Config) *gorm.DB {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		cfg.DB.Username,
		cfg.DB.Password,
		cfg.DB.Host,
		cfg.DB.Port,
		cfg.DB.Database,
	)

	log.Printf("Initialing database with dsn: %v", strings.Replace(dsn, cfg.DB.Password, strings.Repeat("x", len(cfg.DB.Password)), 1))

	dial := mysql.Open(dsn)
	db, err := gorm.Open(dial, &gorm.Config{})
	if err != nil {
		log.Panic(err.Error())
	}

	log.Println("connecting to database successfully")

	return db
}
