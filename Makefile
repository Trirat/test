# รันแอปพลิเคชัน Go โดยใช้คำสั่ง "go run"
run:
	 go run cmd/*.go

# สร้าง Docker image โดยใช้ Dockerfile และกำหนดชื่อ image เป็น "app-attraction:1.0.0"
build-dockerfile:
	 docker build -t app-attraction:1.0.0 . 

# รัน Docker container โดยใช้ Docker image "app-attraction:1.0.0" และตั้งชื่อ container เป็น "test-app" พร้อมกับการเชื่อมโยงพอร์ต 8081
docker-run:
	 docker run --name=test-app -d -p 8081:8081 -d app-attraction:1.0.0

dcp-run:
	docker compose -f docker-compose-mysql.yaml up -d

