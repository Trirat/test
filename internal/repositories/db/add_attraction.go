package db

import (
	"fmt"
	"test/internal/models"

	"log"

	"gorm.io/gorm"
)

type dbRepository struct {
	db *gorm.DB
}

func NewDBRepository(db *gorm.DB) DBRepository {
	return &dbRepository{db: db}
}

func (r *dbRepository) AddAttraction(req models.Attraction) error {
	tx := r.db.Create(&req)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return tx.Error
	}

	log.Println("create data attraction successfully")
	return nil
}
