package db

import (
	"fmt"
	"test/internal/models"
)

func (r *dbRepository) UpdateAttraction(req models.Attraction) (models.Attraction, error) {
	data := models.Attraction{}
	err := r.db.Find(&data).Where(`id = ?`, req.Id)
	if err.Error != nil {
		fmt.Println(err.Error)
		return models.Attraction{}, err.Error
	}
	data.Id = req.Id
	data.Name = req.Name
	data.Detail = req.Detail
	data.Coverimage = req.Coverimage
	data.Latitude = req.Latitude
	data.Longitude = req.Longitude

	tx := r.db.Save(&data)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return models.Attraction{}, tx.Error
	}

	return data, nil
}

func (r *dbRepository) UpdateAttraction2(req models.Attraction) (models.Attraction, error) {
	res := models.Attraction{}
	query := "UPDATE attractions SET name = ?, detail = ?, coverimage = ?, latitude = ?, longitude = ? WHERE id = ?"
	tx := r.db.Exec(query, req.Name, req.Detail, req.Coverimage, req.Latitude, req.Longitude, req.Id).Model(&res)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return models.Attraction{}, tx.Error
	}
	return res, nil
}
