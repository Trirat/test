package db

import (
	"fmt"
	"test/internal/models"
)

func (r *dbRepository) GetAllAttraction() ([]models.Attraction, error) {
	attractions := []models.Attraction{}
	tx := r.db.Find(&attractions)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return []models.Attraction{}, tx.Error
	}

	return attractions, nil
}

func (r *dbRepository) GetAttractionByName(name string) (models.Attraction, error) {
	attraction := models.Attraction{}
	tx := r.db.Find(&attraction).Where(`name = ?`, name)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return models.Attraction{}, tx.Error
	}

	return attraction, nil
}
