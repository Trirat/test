package db

import (
	"fmt"
	"test/internal/models"
)

func (r *dbRepository) DeleteAttraction(id int) error {
	tx := r.db.Delete(&models.Attraction{}, id)
	if tx.Error != nil {
		fmt.Println(tx.Error)
		return tx.Error
	}

	return nil
}
