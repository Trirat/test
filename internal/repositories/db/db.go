package db

import "test/internal/models"

type DBRepository interface {
	AddAttraction(req models.Attraction) error
	GetAllAttraction() ([]models.Attraction, error)
	GetAttractionByName(name string) (models.Attraction, error)
	UpdateAttraction(req models.Attraction) (models.Attraction, error)
	UpdateAttraction2(req models.Attraction) (models.Attraction, error)
	DeleteAttraction(id int) error
}
