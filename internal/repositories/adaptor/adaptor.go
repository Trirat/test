package adaptor

import "context"

type ExternalAdaptor interface {
	TestHealthCheck(ctx context.Context) (interface{}, error)
}
