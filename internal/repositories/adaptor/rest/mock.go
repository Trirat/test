package rest

import (
	"context"

	"github.com/stretchr/testify/mock"
)

type MockExternalAdaptor struct {
	mock.Mock
}

func (m *MockExternalAdaptor) TestHealthCheck(ctx context.Context) (interface{}, error) {
	args := m.Called(ctx)
	return args.Get(0).(interface{}), args.Error(1)
}
