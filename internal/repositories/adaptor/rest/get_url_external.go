package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"test/config"
	"test/internal/repositories/adaptor"
)

type externalAdaptor struct {
	cfg config.Config
}

func NewExternalAdaptor(cfg config.Config) adaptor.ExternalAdaptor {
	return &externalAdaptor{cfg: cfg}
}

func (s *externalAdaptor) TestHealthCheck(ctx context.Context) (interface{}, error) {
	client := &http.Client{}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8080", nil)
	if err != nil {
		return nil, err
	}

	httpResp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer httpResp.Body.Close()

	bytesResponse, err := io.ReadAll(httpResp.Body)
	if err != nil {
		return nil, err
	}

	if httpResp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s returned HTTP Status: %s", "http://localhost:8080", httpResp.Status)
	}

	var responseData interface{}
	if err := json.Unmarshal(bytesResponse, &responseData); err != nil {
		return nil, err
	}

	return responseData, nil
}

// jsonData := []byte(`{
// 	"name": "John",
// 	"age": 30,
// 	"isStudent": false,
// 	"grades": [95, 88, 75]
// }`)

// // ประกาศตัวแปร responseData แบบ interface{}
// var responseData interface{}

// // แปลงข้อมูล JSON เข้า responseData
// err := json.Unmarshal(jsonData, &responseData)
// if err != nil {
// 	fmt.Println("เกิดข้อผิดพลาดในการแปลง JSON:", err)
// 	return nil, nil
// }
