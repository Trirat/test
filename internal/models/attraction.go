package models

type Attraction struct {
	Id         int64   `json:"id"`
	Name       string  `json:"name"`
	Detail     string  `json:"detail"`
	Coverimage string  `json:"coverimage"`
	Latitude   float64 `json:"latitude"`
	Longitude  float64 `json:longitude"`
}
