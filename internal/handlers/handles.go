package handlers

import (
	"github.com/labstack/echo/v4"
)

type AttractionHandlers interface {
	GetAllAttraction(c echo.Context) error
	GetAttractionByName(c echo.Context) error
	AddAttraction(c echo.Context) error
	UpdateAttraction(c echo.Context) error
	UpdateAttraction2(c echo.Context) error
	DeleteAttraction(c echo.Context) error
}

type ExternalHandlers interface {
	GetTest(c echo.Context) error
}
