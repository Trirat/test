package rest

import (
	"log"
	"net/http"
	"test/internal/handlers"
	"test/internal/service"

	"github.com/labstack/echo/v4"
)

type externalHandler struct {
	svc service.AdaptorService
}

func NewExternalHandler(svc service.AdaptorService) handlers.ExternalHandlers {
	return &externalHandler{svc: svc}
}

func (h *externalHandler) GetTest(c echo.Context) error {
	log.Println("start service get external")
	resp, err := h.svc.Test()
	if err != nil {
		log.Printf("error service get external : %v", err)
		return err
	}
	log.Println("end service get external")
	return c.JSONPretty(http.StatusOK, resp, "")
}
