package rest

import (
	"net/http"
	"test/internal/service"

	"log"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type readinessCheck struct {
	db *gorm.DB
}

func NewReadinessCheck(db *gorm.DB) readinessCheck {
	return readinessCheck{db: db}
}

func InitRouter(rc readinessCheck, svc service.DatabaseService, adaptor service.AdaptorService) *echo.Echo {
	e := echo.New()
	e.GET("/health", healthCheck)
	e.GET("/ready", rc.readiness)
	api := NewAttractionHandler(svc)
	e.GET("/", api.GetAllAttraction)
	e.GET("/:name", api.GetAttractionByName)
	e.POST("/add", api.AddAttraction)
	e.PUT("/update", api.UpdateAttraction)
	e.PUT("/update2", api.UpdateAttraction2)
	e.DELETE("/delete", api.DeleteAttraction)

	external := NewExternalHandler(adaptor)
	e.GET("/external", external.GetTest)

	return e
}

func healthCheck(c echo.Context) error {
	return c.JSONPretty(http.StatusOK, echo.Map{"message": "Service is Running !!"}, "	")
}

func (rc readinessCheck) readiness(c echo.Context) error {

	// standard response error
	var stdResponseError func() error = func() error {
		return c.JSONPretty(http.StatusServiceUnavailable, echo.Map{"message": "Service unavailable"}, "	")
	}

	sqlDB, err := rc.db.DB()
	if err != nil {
		log.Printf("cannot convert *gorm.DB to *sql.DB : %v", err)
		return stdResponseError()
	}
	// Ping db
	if err = sqlDB.Ping(); err != nil {
		log.Printf("cannot ping sql db : %v", err)
		return stdResponseError()
	}

	return c.JSONPretty(http.StatusOK, echo.Map{"message": "Service is ready!!"}, "	")
}
