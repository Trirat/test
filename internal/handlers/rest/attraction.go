package rest

import (
	"log"
	"net/http"
	"strconv"
	"test/internal/handlers"
	"test/internal/models"
	"test/internal/service"

	"github.com/labstack/echo/v4"
)

type attractionHandler struct {
	svc service.DatabaseService
}

func NewAttractionHandler(svc service.DatabaseService) handlers.AttractionHandlers {
	return attractionHandler{svc: svc}
}

func (h attractionHandler) GetAllAttraction(c echo.Context) error {
	log.Println("start service get all attraction")
	resp, err := h.svc.GetAllAttractions()
	if err != nil {
		log.Printf("error service get all attraction : %v", err)
		return err
	}
	log.Println("end service get all attraction")
	return c.JSONPretty(http.StatusOK, resp, "")
}

func (h attractionHandler) GetAttractionByName(c echo.Context) error {
	log.Println("start service get attraction by name")
	name := c.Param("name")
	if name == "" {
		return c.JSONPretty(http.StatusBadRequest, "name is invalid", "")
	}
	resp, err := h.svc.GetAttractionByName(name)
	if err != nil {
		log.Printf("error service get get attraction by name : %v", err)
		return err
	}
	log.Println("end service get get attraction by name")
	return c.JSONPretty(http.StatusOK, resp, "")
}

func (h attractionHandler) AddAttraction(c echo.Context) error {
	log.Println("start service add attraction")
	req := models.Attraction{}
	err := c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	err = h.svc.AddAttraction(req)
	if err != nil {
		log.Printf("error service add attraction : %v", err)
		return err
	}
	log.Println("end service add attraction")
	return c.JSONPretty(http.StatusOK, "add attraction success", "")
}

func (h attractionHandler) UpdateAttraction(c echo.Context) error {
	log.Println("start service update attraction")
	req := models.Attraction{}
	err := c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	resp, err := h.svc.UpdateAttraction(req)
	if err != nil {
		log.Printf("error service update attraction : %v", err)
		return err
	}
	log.Println("end service update attraction")
	return c.JSONPretty(http.StatusOK, resp, "")
}

func (h attractionHandler) UpdateAttraction2(c echo.Context) error {
	log.Println("start service update attraction")
	req := models.Attraction{}
	err := c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	resp, err := h.svc.UpdateAttraction2(req)
	if err != nil {
		log.Printf("error service update attraction : %v", err)
		return err
	}
	log.Println("end service update attraction")
	return c.JSONPretty(http.StatusOK, resp, "")
}

func (h attractionHandler) DeleteAttraction(c echo.Context) error {
	id, err := strconv.Atoi(c.QueryParam("id"))
	if err != nil {
		log.Printf("error service delete attraction : %v", err)
		return err
	}
	if id == 0 {
		return c.JSONPretty(http.StatusBadRequest, "id is invalid", "")
	}

	err = h.svc.DeleteAttraction(id)
	if err != nil {
		log.Printf("error service delete attraction : %v", err)
		return err
	}
	log.Println("end service delete attraction")
	return c.JSONPretty(http.StatusOK, "delete data attraction success", "")
}
