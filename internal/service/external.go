package service

import (
	"context"
	"test/internal/repositories/adaptor"
)

type externalService struct {
	svc adaptor.ExternalAdaptor
}

func NewExternalServices(svc adaptor.ExternalAdaptor) AdaptorService {
	return &externalService{svc: svc}
}

func (s *externalService) Test() (interface{}, error) {
	res, err := s.svc.TestHealthCheck(context.Background())
	if err != nil {
		return nil, err
	}
	return res, nil
}
