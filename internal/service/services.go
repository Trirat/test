package service

import "test/internal/models"

type DatabaseService interface {
	GetAllAttractions() ([]models.Attraction, error)
	GetAttractionByName(name string) (models.Attraction, error)
	AddAttraction(req models.Attraction) error
	UpdateAttraction(req models.Attraction) (models.Attraction, error)
	UpdateAttraction2(req models.Attraction) (models.Attraction, error)
	DeleteAttraction(id int) error
}

type AdaptorService interface {
	Test() (interface{}, error)
}
