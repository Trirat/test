package service

import (
	"test/internal/models"
	"test/internal/repositories/db"
)

type dbService struct {
	db db.DBRepository
}

func NewDBServices(db db.DBRepository) DatabaseService {
	return &dbService{db: db}
}

func (s *dbService) GetAllAttractions() ([]models.Attraction, error) {
	res, err := s.db.GetAllAttraction()
	if err != nil {
		return []models.Attraction{}, err
	}

	return res, nil
}

func (s *dbService) GetAttractionByName(name string) (models.Attraction, error) {
	res, err := s.db.GetAttractionByName(name)
	if err != nil {
		return models.Attraction{}, err
	}
	return res, nil
}

func (s *dbService) AddAttraction(req models.Attraction) error {
	err := s.db.AddAttraction(req)
	if err != nil {
		return err
	}
	return nil
}

func (s *dbService) UpdateAttraction(req models.Attraction) (models.Attraction, error) {
	res, err := s.db.UpdateAttraction(req)
	if err != nil {
		return models.Attraction{}, err
	}
	return res, nil
}

func (s *dbService) UpdateAttraction2(req models.Attraction) (models.Attraction, error) {
	res, err := s.db.UpdateAttraction(req)
	if err != nil {
		return models.Attraction{}, err
	}
	return res, nil
}

func (s *dbService) DeleteAttraction(id int) error {
	err := s.db.DeleteAttraction(id)
	if err != nil {
		return err
	}
	return nil
}
