package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"test/config"
	"test/internal/handlers/rest"
	adaptor "test/internal/repositories/adaptor/rest"
	repo "test/internal/repositories/db"
	"test/internal/service"
	"time"
)

func main() {
	ctx := context.Background()
	cfg := config.InitConfig()
	db := config.InitDatabase(cfg)

	dbRepo := repo.NewDBRepository(db) //service from database
	svc := service.NewDBServices(dbRepo)

	adaptor := adaptor.NewExternalAdaptor(cfg) //service from adaptor
	external := service.NewExternalServices(adaptor)

	app := rest.InitRouter(rest.NewReadinessCheck(db), svc, external)
	go func() {
		if err := app.Start(fmt.Sprint(":", cfg.App.Port)); err != nil {
			log.Printf("listen server error: %v", err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	<-c
	log.Println("Gracefully shutting down...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := app.Shutdown(ctx); err != nil {
		log.Printf("Failed to gracefully shut down the applicatio: %v", err)
	}

	log.Println("Running cleanup tasks...")
	log.Println("successful shutdown.")
}
