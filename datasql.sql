CREATE TABLE attractions (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    detail TEXT,
    coverimage VARCHAR(255),
    latitude FLOAT,
    longitude FLOAT
);

NSERT INTO attractions (name, detail, coverimage, latitude, longitude)
VALUES ('ชื่อสถานที่', 'รายละเอียดของสถานที่', 'ชื่อไฟล์รูปภาพ.jpg', 13.7563, 100.5018);